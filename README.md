Welcome to the wisla repository. Wisla is a addon for waila with a optional dependency for wawla to add support for the Lord of the Rings mod.

__Instalation__

You can download the file from [curse forge](https://www.curseforge.com/minecraft/mc-mods/wisla "Wisla curse forge page.") or from gitlab in the release tab. If you do download it from gitlab you have to unzip it from the zipped artifact file.

To run wisla you will need 2 other mods. [Waila](https://www.curseforge.com/minecraft/mc-mods/waila "Waila curse forge page.") the main tooltip mod and the [LOTR Mod](https://lotrminecraftmod.fandom.com/wiki/The_Lord_of_the_Rings_Minecraft_Mod_Wiki "LOTR Mod wiki page.") by Mevans. If you want you can also install [Wawla](https://www.curseforge.com/minecraft/mc-mods/wawla-what-are-we-looking-at "Wawla curse forge page.") a addon for waila to display lotr creatures speeds.

This mod is a forge mod which runs on 1.7.10.


__Bug & Suggestions__

Any suggestions and or bug reports are welcome in the issues tab.

__Credits__

Credits to Nakras for making the logo!