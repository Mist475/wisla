package io.gitlab.dwarfyassassin.wisla.addon.lotr;

import lotr.common.LOTRMod;
import lotr.common.block.LOTRBlockArmorStand;
import lotr.common.block.LOTRBlockBed;
import lotr.common.block.LOTRBlockBookshelfStorage;
import lotr.common.block.LOTRBlockDoubleFlower;
import lotr.common.block.LOTRBlockDoubleTorch;
import lotr.common.block.LOTRBlockFlaxCrop;
import lotr.common.block.LOTRBlockGrapevine;
import lotr.common.block.LOTRBlockGrapevineRed;
import lotr.common.block.LOTRBlockGrapevineWhite;
import lotr.common.block.LOTRBlockLeavesBase;
import lotr.common.block.LOTRBlockLeavesVanilla1;
import lotr.common.block.LOTRBlockLeavesVanilla2;
import lotr.common.block.LOTRBlockLeekCrop;
import lotr.common.block.LOTRBlockLettuceCrop;
import lotr.common.block.LOTRBlockPipeweedCrop;
import lotr.common.block.LOTRBlockPlaceableFood;
import lotr.common.block.LOTRBlockPlate;
import lotr.common.block.LOTRBlockSlabBase;
import lotr.common.block.LOTRBlockTurnipCrop;
import lotr.common.block.LOTRBlockWoodBeam;
import lotr.common.block.LOTRBlockYamCrop;
import mcp.mobius.waila.api.IWailaConfigHandler;
import mcp.mobius.waila.api.IWailaDataAccessor;
import mcp.mobius.waila.api.IWailaDataProvider;
import mcp.mobius.waila.api.IWailaRegistrar;
import mcp.mobius.waila.api.SpecialChars;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

import java.util.List;

import static io.gitlab.dwarfyassassin.wisla.util.GeneralUtils.translateUtil;

public class AddonLOTRBlocks implements IWailaDataProvider {
    @Override
    public ItemStack getWailaStack(IWailaDataAccessor accessor, IWailaConfigHandler config) {
        Block block = accessor.getBlock();

        if (block instanceof LOTRBlockBed blockBed) {
            return new ItemStack(blockBed.getItem(accessor.getWorld(), 0, 0, 0), 0, accessor.getMetadata());
        } else if (block instanceof LOTRBlockDoubleFlower && accessor.getMetadata() > 8) {
            int x = accessor.getPosition().blockX;
            int y = accessor.getPosition().blockY - 1;
            int z = accessor.getPosition().blockZ;
            int meta = accessor.getWorld().getBlockMetadata(x, y, z);
            return new ItemStack(Item.getItemFromBlock(block), 0, meta);
        } else if (block instanceof LOTRBlockDoubleTorch) {
            return new ItemStack(Item.getItemFromBlock(block));
        } else if (block instanceof LOTRBlockGrapevineRed) {
            return new ItemStack(LOTRMod.grapeRed);
        } else if (block instanceof LOTRBlockGrapevineWhite) {
            return new ItemStack(LOTRMod.grapeWhite);
        } else if (block instanceof LOTRBlockPlate plateBlock) {
            return new ItemStack(plateBlock.getItemDropped(0, accessor.getWorld().rand, 0));
        } else if (block instanceof LOTRBlockArmorStand) {
            return new ItemStack(LOTRMod.armorStandItem);
        }

        return accessor.getStack();
    }

    @Override
    public List<String> getWailaHead(ItemStack itemStack, List<String> tip, IWailaDataAccessor accessor, IWailaConfigHandler config) {
        Block block = accessor.getBlock();

        if (block instanceof LOTRBlockDoubleFlower && (accessor.getMetadata() & 8) != 0) {
            int x = accessor.getPosition().blockX;
            int y = accessor.getPosition().blockY - 1;
            int z = accessor.getPosition().blockZ;
            int meta = accessor.getWorld().getBlockMetadata(x, y, z);
            if (accessor.getWorld().getBlock(x, y, z) instanceof LOTRBlockDoubleFlower flowerToUse) {
                translateUtil(tip, flowerToUse.getUnlocalizedName() + "." + meta + ".name");
            }
        } else if (block instanceof LOTRBlockLeavesBase && accessor.getMetadata() > 3) {
            var moduleMeta = accessor.getMetadata() % 4;
            if (block instanceof LOTRBlockLeavesVanilla1) {
                translateUtil(tip, "tile.leaves." + switch (moduleMeta) {
                    case 0 -> "oak";
                    case 1 -> "spruce";
                    case 2 -> "birch";
                    case 3 -> "jungle";
                    default -> "";
                } + ".name");
            } else if (block instanceof LOTRBlockLeavesVanilla2) {
                translateUtil(tip, "tile.leaves." + switch (moduleMeta) {
                    case 0 -> "acacia";
                    case 1 -> "big_oak";
                    default -> "";
                } + ".name");
            } else {
                translateUtil(tip, block.getUnlocalizedName() + "." + (moduleMeta) + ".name");
            }
        } else if (block instanceof LOTRBlockWoodBeam) {
            translateUtil(tip, block.getUnlocalizedName() + "." + (accessor.getMetadata() % 4) + ".name");
        } else if (block instanceof LOTRBlockSlabBase && accessor.getMetadata() > 7) {
            translateUtil(tip, block.getUnlocalizedName() + "." + (accessor.getMetadata() - 8) + ".name");
        } else if (block instanceof LOTRBlockBookshelfStorage) {
            translateUtil(tip, "tile.bookshelf.name");
        } else if (block instanceof LOTRBlockGrapevineRed) {
            translateUtil(tip, "item.lotr:grapeRed.name");
        } else if (block instanceof LOTRBlockGrapevineWhite) {
            translateUtil(tip, "item.lotr:grapeWhite.name");
        } else if (block instanceof LOTRBlockArmorStand) {
            translateUtil(tip, "item.lotr:armorStand.name");
        } else if (
            block instanceof LOTRBlockPlaceableFood ||
            block instanceof LOTRBlockYamCrop ||
            block instanceof LOTRBlockTurnipCrop ||
            block instanceof LOTRBlockPipeweedCrop ||
            block instanceof LOTRBlockLettuceCrop ||
            block instanceof LOTRBlockLeekCrop ||
            block instanceof LOTRBlockFlaxCrop ||
            block instanceof LOTRBlockBed ||
            block instanceof LOTRBlockPlate ||
            block instanceof LOTRBlockDoubleTorch
        ) {
            var splitString = tip.get(0).replaceFirst("..tile.", "item.").split("\\.name");
            // Enhanced tooltips enabled || In some cases the name is already present
            if (splitString.length == 2 || !splitString[0].startsWith(SpecialChars.WHITE)) {
                translateUtil(tip, splitString[0] + ".name");
            }
        }

        return tip;
    }

    @Override
    public List<String> getWailaBody(ItemStack itemStack, List<String> tip, IWailaDataAccessor accessor, IWailaConfigHandler config) {
        return tip;
    }

    @Override
    public List<String> getWailaTail(ItemStack itemStack, List<String> tip, IWailaDataAccessor accessor, IWailaConfigHandler config) {
        return tip;
    }

    @Override
    public NBTTagCompound getNBTData(EntityPlayerMP player, TileEntity te, NBTTagCompound tag, World world, int x, int y, int z) {
        return tag;
    }

    @SuppressWarnings("unused")
    public static void registerAddon(IWailaRegistrar register) {
        AddonLOTRBlocks dataProvider = new AddonLOTRBlocks();

        register.registerHeadProvider(dataProvider, LOTRBlockArmorStand.class);
        register.registerHeadProvider(dataProvider, LOTRBlockBed.class);
        register.registerHeadProvider(dataProvider, LOTRBlockBookshelfStorage.class);
        register.registerHeadProvider(dataProvider, LOTRBlockDoubleFlower.class);
        register.registerHeadProvider(dataProvider, LOTRBlockDoubleTorch.class);
        register.registerHeadProvider(dataProvider, LOTRBlockFlaxCrop.class);
        register.registerHeadProvider(dataProvider, LOTRBlockGrapevine.class);
        register.registerHeadProvider(dataProvider, LOTRBlockLeavesBase.class);
        register.registerHeadProvider(dataProvider, LOTRBlockLeekCrop.class);
        register.registerHeadProvider(dataProvider, LOTRBlockLettuceCrop.class);
        register.registerHeadProvider(dataProvider, LOTRBlockPipeweedCrop.class);
        register.registerHeadProvider(dataProvider, LOTRBlockPlaceableFood.class);
        register.registerHeadProvider(dataProvider, LOTRBlockPlate.class);
        register.registerHeadProvider(dataProvider, LOTRBlockSlabBase.class);
        register.registerHeadProvider(dataProvider, LOTRBlockTurnipCrop.class);
        register.registerHeadProvider(dataProvider, LOTRBlockWoodBeam.class);
        register.registerHeadProvider(dataProvider, LOTRBlockYamCrop.class);

        register.registerStackProvider(dataProvider, LOTRBlockArmorStand.class);
        register.registerStackProvider(dataProvider, LOTRBlockBed.class);
        register.registerStackProvider(dataProvider, LOTRBlockDoubleFlower.class);
        register.registerStackProvider(dataProvider, LOTRBlockGrapevine.class);
        register.registerStackProvider(dataProvider, LOTRBlockPlate.class);
    }
}
