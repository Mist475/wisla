package io.gitlab.dwarfyassassin.wisla.addon.wawla;

import java.util.List;
import io.gitlab.dwarfyassassin.wisla.util.MobUtils;
import lotr.common.entity.npc.LOTREntitySpiderBase;
import lotr.common.entity.npc.LOTREntityWarg;
import mcp.mobius.waila.api.IWailaConfigHandler;
import mcp.mobius.waila.api.IWailaEntityAccessor;
import mcp.mobius.waila.api.IWailaEntityProvider;
import mcp.mobius.waila.api.IWailaRegistrar;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;

public class AddonWawlaLOTREntities implements IWailaEntityProvider {
    // Wawla config string
    private static final String CONFIG_ENTITY_SPEED = "wawla.horse.showSpeed";

    @Override
    public NBTTagCompound getNBTData(EntityPlayerMP player, Entity entity, NBTTagCompound tag, World world) {
        if (entity != null) {
            entity.writeToNBT(tag);
        }
        return tag;
    }

    @Override
    public List<String> getWailaHead(Entity entity, List<String> tip, IWailaEntityAccessor data, IWailaConfigHandler cfg) {
        return tip;
    }

    @Override
    public List<String> getWailaBody(Entity entity, List<String> tip, IWailaEntityAccessor data, IWailaConfigHandler cfg) {
        if (entity instanceof LOTREntityWarg) {
            if (cfg.getConfig(CONFIG_ENTITY_SPEED)) {
                tip.add(StatCollector.translateToLocal("tooltip.wawla.speed") + ": " + MobUtils.getRoundMobSpeed((LOTREntityWarg) entity));
            }
        }
        else if (entity instanceof LOTREntitySpiderBase spider) {
            if (cfg.getConfig(CONFIG_ENTITY_SPEED)) {
                tip.add(StatCollector.translateToLocal("tooltip.wawla.speed") + ": " + MobUtils.getRoundMobSpeed(spider));
            }
        }

        return tip;
    }

    @Override
    public List<String> getWailaTail(Entity entity, List<String> tip, IWailaEntityAccessor data, IWailaConfigHandler cfg) {
        return tip;
    }

    @Override
    public Entity getWailaOverride(IWailaEntityAccessor data, IWailaConfigHandler cfg) {
        return data.getEntity();
    }

    @SuppressWarnings("unused")
    public static void registerAddon(IWailaRegistrar register) {
        AddonWawlaLOTREntities dataProvider = new AddonWawlaLOTREntities();

        register.registerBodyProvider(dataProvider, LOTREntityWarg.class);
        register.registerNBTProvider(dataProvider, LOTREntityWarg.class);
        register.registerBodyProvider(dataProvider, LOTREntitySpiderBase.class);
    }

}
