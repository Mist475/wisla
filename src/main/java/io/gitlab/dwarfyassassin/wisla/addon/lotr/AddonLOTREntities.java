package io.gitlab.dwarfyassassin.wisla.addon.lotr;

import java.util.List;
import io.gitlab.dwarfyassassin.wisla.util.PlayerUtils;
import lotr.common.LOTRBannerProtection;
import lotr.common.LOTRBannerProtection.ProtectType;
import lotr.common.entity.item.LOTREntityBanner;
import lotr.common.entity.item.LOTREntityBannerWall;
import lotr.common.entity.npc.LOTREntitySpiderBase;
import mcp.mobius.waila.api.IWailaConfigHandler;
import mcp.mobius.waila.api.IWailaEntityAccessor;
import mcp.mobius.waila.api.IWailaEntityProvider;
import mcp.mobius.waila.api.IWailaRegistrar;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;

public class AddonLOTREntities implements IWailaEntityProvider {
    private static final String CONFIG_SPIDER_SIZE = "wisla.spider.size";
    private static final String CONFIG_SPIDER_VENOM = "wisla.spider.venom";
    private static final String CONFIG_BANNER_OWNER = "wisla.banner.owner";
    private static final String CONFIG_BANNER_MODE = "wisla.banner.mode";
    private static final String CONFIG_BANNER_INTERACT = "wisla.banner.interact";

    @Override
    public NBTTagCompound getNBTData(EntityPlayerMP player, Entity entity, NBTTagCompound tag, World world) {
        if (entity != null) {
            entity.writeToNBT(tag);
        }
        return tag;
    }

    @Override
    public List<String> getWailaHead(Entity entity, List<String> tip, IWailaEntityAccessor data, IWailaConfigHandler cfg) {
        return tip;
    }

    @Override
    public List<String> getWailaBody(Entity entity, List<String> tip, IWailaEntityAccessor data, IWailaConfigHandler cfg) {
        if (entity instanceof LOTREntitySpiderBase spider) {
            if (cfg.getConfig(CONFIG_SPIDER_SIZE)) {
                tip.add(StatCollector.translateToLocal("tooltip.wisla.spider.size") + ": " + spider.getSpiderScale());
            }
            if (cfg.getConfig(CONFIG_SPIDER_VENOM)) {
                tip.add(StatCollector.translateToLocal("tooltip.wisla.spider.venom") + ": " + StatCollector.translateToLocal("tooltip.wisla.spider.venom." + spider.getSpiderType()));
            }
        }
        else if (entity instanceof LOTREntityBanner banner) {
            if(banner.isProtectingTerritory()) {
                if (cfg.getConfig(CONFIG_BANNER_OWNER)) {
                    tip.add(StatCollector.translateToLocal("tooltip.wisla.banner.owner") + ": " + PlayerUtils.getPlayerNameFromUUID(banner.getPlacingPlayer().getId()));
                }
                if (cfg.getConfig(CONFIG_BANNER_MODE)) {
                    if(banner.isStructureProtection()) {
                        tip.add(StatCollector.translateToLocal("tooltip.wisla.banner.mode") + ": " + StatCollector.translateToLocal("tooltip.wisla.banner.mode.structure"));
                    }
                    else if(banner.isPlayerSpecificProtection()) {
                        tip.add(StatCollector.translateToLocal("tooltip.wisla.banner.mode") + ": " + StatCollector.translateToLocal("tooltip.wisla.banner.mode.player"));
                    }
                    else {
                        tip.add(StatCollector.translateToLocal("tooltip.wisla.banner.mode") + ": " + StatCollector.translateToLocal("tooltip.wisla.banner.mode.faction"));
                        tip.add(StatCollector.translateToLocal("tooltip.wisla.banner.mode.faction.alignment") + ": " + banner.getAlignmentProtection());
                    }
                }
                if (cfg.getConfig(CONFIG_BANNER_INTERACT)) {
                    tip.add(StatCollector.translateToLocal("tooltip.wisla.banner.interact") + ": " + (LOTRBannerProtection.forPlayer(Minecraft.getMinecraft().thePlayer).protects(banner) != ProtectType.NONE));
                }
            }
        }

        return tip;
    }

    @Override
    public List<String> getWailaTail(Entity entity, List<String> tip, IWailaEntityAccessor data, IWailaConfigHandler cfg) {
        return tip;
    }

    @Override
    public Entity getWailaOverride(IWailaEntityAccessor data, IWailaConfigHandler cfg) {
        return data.getEntity();
    }

    @SuppressWarnings("unused")
    public static void registerAddon(IWailaRegistrar register) {
        AddonLOTREntities dataProvider = new AddonLOTREntities();

        register.registerBodyProvider(dataProvider, LOTREntitySpiderBase.class);
        register.registerNBTProvider(dataProvider, LOTREntitySpiderBase.class);
        register.registerBodyProvider(dataProvider, LOTREntityBanner.class);
        register.registerNBTProvider(dataProvider, LOTREntityBanner.class);
        register.registerBodyProvider(dataProvider, LOTREntityBannerWall.class);
        register.registerNBTProvider(dataProvider, LOTREntityBannerWall.class);

        register.addConfig("Wisla-Entity", CONFIG_SPIDER_SIZE);
        register.addConfig("Wisla-Entity", CONFIG_SPIDER_VENOM);
        register.addConfig("Wisla-Entity", CONFIG_BANNER_OWNER);
        register.addConfig("Wisla-Entity", CONFIG_BANNER_MODE);
        register.addConfig("Wisla-Entity", CONFIG_BANNER_INTERACT);
    }

}
