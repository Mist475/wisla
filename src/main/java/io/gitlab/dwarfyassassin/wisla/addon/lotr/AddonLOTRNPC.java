package io.gitlab.dwarfyassassin.wisla.addon.lotr;

import java.util.List;
import io.gitlab.dwarfyassassin.wisla.util.PlayerUtils;
import lotr.common.entity.npc.LOTREntityNPC;
import lotr.common.entity.npc.LOTRHiredNPCInfo;
import mcp.mobius.waila.api.IWailaConfigHandler;
import mcp.mobius.waila.api.IWailaEntityAccessor;
import mcp.mobius.waila.api.IWailaEntityProvider;
import mcp.mobius.waila.api.IWailaRegistrar;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;

public class AddonLOTRNPC implements IWailaEntityProvider {
    private static final String CONFIG_NPC_HIRED_OWNER = "wisla.hired.owner";

    @Override
    public NBTTagCompound getNBTData(EntityPlayerMP player, Entity entity, NBTTagCompound tag, World world) {
        if (entity != null) {
            entity.writeToNBT(tag);
        }
        return tag;
    }

    @Override
    public List<String> getWailaHead(Entity entity, List<String> tip, IWailaEntityAccessor data, IWailaConfigHandler cfg) {
        return tip;
    }

    @Override
    public List<String> getWailaBody(Entity entity, List<String> tip, IWailaEntityAccessor data, IWailaConfigHandler cfg) {
        if (entity instanceof LOTREntityNPC npc) {
            LOTRHiredNPCInfo info = npc.hiredNPCInfo;
            String name = null;

            if(info.getHiringPlayer() != null) {
                name = info.getHiringPlayer().getDisplayName();
            }
            else if(info.getHiringPlayerUUID() != null) {
                name = PlayerUtils.getPlayerNameFromUUID(info.getHiringPlayerUUID());
            }

            if (cfg.getConfig(CONFIG_NPC_HIRED_OWNER) && name != null) {
                tip.add(StatCollector.translateToLocal("tooltip.wisla.hired.owner") + ": " + name);
            }
        }

        return tip;
    }

    @Override
    public List<String> getWailaTail(Entity entity, List<String> tip, IWailaEntityAccessor data, IWailaConfigHandler cfg) {
        return tip;
    }

    @Override
    public Entity getWailaOverride(IWailaEntityAccessor data, IWailaConfigHandler cfg) {
        return data.getEntity();
    }

    @SuppressWarnings("unused")
    public static void registerAddon(IWailaRegistrar register) {
        AddonLOTRNPC dataProvider = new AddonLOTRNPC();

        register.registerBodyProvider(dataProvider, LOTREntityNPC.class);
        register.registerNBTProvider(dataProvider, LOTREntityNPC.class);

        register.addConfig("Wisla-Entity", CONFIG_NPC_HIRED_OWNER);
    }

}
