package io.gitlab.dwarfyassassin.wisla.util;

import java.util.UUID;
import net.minecraftforge.common.UsernameCache;

public class PlayerUtils {

    public static String getPlayerNameFromUUID(UUID uuid) {
        if(uuid == null) return null;

        String userName = UsernameCache.getLastKnownUsername(uuid);
        if(userName != null) return userName;

        return uuid.toString();
    }

}
