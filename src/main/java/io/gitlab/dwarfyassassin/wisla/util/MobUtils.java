package io.gitlab.dwarfyassassin.wisla.util;

import io.gitlab.dwarfyassassin.wisla.Wisla;
import net.darkhax.wawla.util.Utilities;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;

public class MobUtils {
    public static double getRoundMobSpeed(EntityLivingBase entity) {
        if(!Wisla.isWawlaLoaded) return 0;
        return Utilities.round(entity.getEntityAttribute(SharedMonsterAttributes.movementSpeed).getAttributeValue(), 4);
    }
}
